package br.com.battlebits.music.server;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Arquivo criado em 30/05/2017.
 * Desenvolvido por:
 *
 * @author Luãn Pereira.
 */
public class Client extends Thread {

    private static long counter;
    private Socket socket;

    public Client(Socket socket) {
        this.socket = socket;
        setName("Client - #" + (++counter));
    }

    @Override
    public void run() {
        try {
            DataInputStream dis = new DataInputStream(socket.getInputStream());
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());

            String str = dis.readUTF();
            if (str.startsWith("{") && str.endsWith("}")) {
                JsonElement element = Server.getParser().parse(str);
                if (element instanceof JsonObject) {
                    JsonObject obj = (JsonObject) element;
                    if (obj.has("action") && obj.has("token") && obj.get("token").getAsString().equals(Server.TOKEN)) {
                        try {
                            String action = obj.get("action").getAsString();
                            switch (action) {
                                case "download": {
                                    if (!obj.has("vid"))
                                        break;
                                    String vid = obj.get("vid").getAsString();
                                    writeOut(dos, handleDownload(vid));
                                    break;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            dos.writeUTF("closed");
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected JsonElement handleDownload(String vid) {
        if (!Server.PENDING.contains(vid)) {
            Server.PENDING.add(vid);


        }


        return null;
    }

    protected void writeOut(DataOutputStream dos, JsonElement element) throws Exception {
        if (element != null) {
            dos.writeUTF(Server.getGson().toJson(element));
        }
    }

}
