package br.com.battlebits.music.server.youtube;

import br.com.battlebits.music.server.Server;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Arquivo criado em 31/05/2017.
 * Desenvolvido por:
 *
 * @author Luãn Pereira.
 */
@RequiredArgsConstructor
public class YoutubeClient {

    @NonNull
    private String key;

    private final Map<String, JsonObject> cache = new HashMap<>();

    public JsonObject request(String vid) throws Exception {
        if (cache.containsKey(vid)) return cache.get(vid);
        URLConnection con = new URL("https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails,topicDetails&id=" + vid + "&key=" + key).openConnection();
        JsonObject json = (JsonObject) Server.getParser().parse(new BufferedReader(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8)));
        if (!json.has("error") && json.has("items")) {
            JsonArray items = json.getAsJsonArray("items");
            if (items.size() > 0) {
                JsonObject item = (JsonObject) items.get(0);
                return cache.computeIfAbsent(vid, v -> item);
            }
        }
        return null;
    }

    public boolean download(File path, String vid) throws IOException, YoutubeException {
        List<String> log = new ArrayList<>();
        Process p = Runtime.getRuntime().exec("youtube-dl -o audio.m4a -f bestaudio[ext=m4a] --force-ipv4 -i https://youtu.be/" + vid, null, path);

        new BufferedReader(new InputStreamReader(p.getInputStream(), StandardCharsets.UTF_8)).lines().forEach(s -> print(System.out, log, s));
        new BufferedReader(new InputStreamReader(p.getErrorStream(), StandardCharsets.UTF_8)).lines().forEach(s -> print(System.err, log, s));

        for (String line : log) {
            if (line.startsWith("ERROR: ")) {
                String message = line.replace("ERROR: ", "");
                throw new YoutubeException(message);
            }
        }

        File m4a = new File(path, "audio.m4a");
        File ogg = new File(path, "audio.ogg");

        if (!m4a.exists() || !m4a.isFile())
            throw new YoutubeException("Download error");

        p = Runtime.getRuntime().exec("ffmpeg -i audio.m4a audio.ogg", null, path);
        new BufferedReader(new InputStreamReader(p.getErrorStream(), StandardCharsets.UTF_8)).lines().forEach(System.err::println);
        new BufferedReader(new InputStreamReader(p.getInputStream(), StandardCharsets.UTF_8)).lines().forEach(System.out::println);

        if (!ogg.exists() || !ogg.isFile())
            throw new YoutubeException("Conversion error");

        return m4a.delete();
    }

    private void print(PrintStream ps, List<String> log, String str) {
        ps.println(str);
        log.add(str);
    }

}
