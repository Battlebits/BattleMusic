package br.com.battlebits.music.server;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response.Status;

import java.io.*;
import java.util.List;
import java.util.Map;

/**
 * Arquivo criado em 30/05/2017.
 * Desenvolvido por:
 *
 * @author Luãn Pereira.
 */
public class ServerWeb extends NanoHTTPD {

    public ServerWeb(int port) {
        super(port);
    }

    @Override
    public Response serve(IHTTPSession session) {
        try {
            Map<String, List<String>> parameters = session.getParameters();
            if (!parameters.isEmpty()) {
                if (parameters.containsKey("vid") && !parameters.get("vid").isEmpty()) {
                    File file = new File(Server.DATA_PATH, parameters.get("vid").get(0) + ".zip");
                    if (file.exists() && file.isFile()) {
                        return newFixedLengthResponse(Status.OK, "application/octet-stream", new FileInputStream(file), file.length());
                    }
                } else if (parameters.containsKey("empty")) {
                    return newFixedLengthResponse(Status.OK, "application/octet-stream", new ByteArrayInputStream(new byte[128]), 128);
                }
            }
            return newFixedLengthResponse(Status.OK, MIME_PLAINTEXT, "OK.");
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            return newFixedLengthResponse(Status.INTERNAL_ERROR, MIME_PLAINTEXT, sw.toString());
        }
    }

}
