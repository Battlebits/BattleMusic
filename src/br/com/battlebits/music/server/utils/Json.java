package br.com.battlebits.music.server.utils;

import com.google.gson.*;
import lombok.Getter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;

/**
 * Arquivo criado em 21/05/2017.
 * Desenvolvido por:
 *
 * @author Luãn Pereira.
 */
public class Json {

    @Getter
    private static final Gson gson = new Gson();

    @Getter
    private static final Gson prettyGson = new GsonBuilder().setPrettyPrinting().create();

    @Getter
    private static final JsonParser parser = new JsonParser();

    public static JsonObject info(String vid, String title, String genre, Duration duration) {
        JsonObject root = new JsonObject();

        root.addProperty("vid", vid);
        root.addProperty("genre", genre);
        root.addProperty("title", title);
        root.addProperty("duration", duration.getSeconds());

        return root;
    }

    public static JsonObject mcmeta(String vid, String desc) {
        JsonObject root = new JsonObject();

        JsonObject pack = new JsonObject();
        pack.addProperty("pack_format", 1);
        pack.addProperty("description", desc);
        root.add("pack", pack);

        return root;
    }

    public static JsonObject sounds(String vid) {
        JsonObject root = new JsonObject();
        JsonObject obj = new JsonObject();
        obj.addProperty("category", "record");

        JsonArray sounds = new JsonArray();
        JsonObject sound = new JsonObject();
        sound.addProperty("name", "server/audio");
        sound.addProperty("stream", true);
        sounds.add(sound);

        obj.add("sounds", sounds);
        root.add(vid, obj);

        return root;
    }

    public static JsonElement read(File f) throws IOException {
        try (FileReader fr = new FileReader(f)) {
            return parser.parse(fr);
        }
    }

    public static void write(File f, JsonElement je) throws IOException {
        if (!f.exists()) {
            File p = f.getParentFile();

            if (!p.exists())
                p.mkdirs();

            f.createNewFile();
        }

        try (FileWriter fw = new FileWriter(f)) {
            fw.write(prettyGson.toJson(je));
            fw.flush();
        }
    }

}
