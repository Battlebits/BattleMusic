package br.com.battlebits.music.server;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import fi.iki.elonen.NanoHTTPD;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

/**
 * Arquivo criado em 30/05/2017.
 * Desenvolvido por:
 *
 * @author Luãn Pereira.
 */
@RequiredArgsConstructor
public class Server extends Thread {

    @Getter
    public static Server instance;

    @Getter
    public static final Gson gson = new Gson();

    @Getter
    public static final JsonParser parser = new JsonParser();

    public static final int WEB_PORT = 80, SERVER_PORT = 1541;
    public static final Set<String> PENDING = new HashSet<>();

    public static final String TOKEN = "";
    public static final String PATH = System.getProperty("user.dir");
    public static final String DATA_PATH = PATH + File.separator + "data" + File.separator;

    public static void main(String[] args) throws Exception {
        ServerWeb web = new ServerWeb(WEB_PORT);
        web.start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
        instance = new Server();
        instance.setName("Server");
        instance.start();
    }

    @Override
    public void run() {
        try {
            try (ServerSocket server = new ServerSocket(SERVER_PORT)) {
                while (!server.isClosed()) {
                    Socket socket = server.accept();
                    new Client(socket).start();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
